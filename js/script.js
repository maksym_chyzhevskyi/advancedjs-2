/* Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
Використовується для того щоб "ловити" помилки в коді і робити певні дії у випадку помилки.
Може використовуватись для взаємодіє з користувачем, або при отриманні даних з сервера.  
*/

const wrapper = document.getElementById('root');
const booksList = document.createElement('ul');
wrapper.append(booksList);

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

class NotEnoughProperties extends Error {
    constructor(property) {
        super()
        this.errorName = 'NotEnoughProperties'
        this.message = `The ${property} property is not specified`
    }
}

class BookInformation {
    constructor({ author, name, price }) {
        this.author = author;
        this.name = name;
        this.price = price;
        this.li = document.createElement('li')
        if (!author || !name || !price) {
            let propertyTitle = 'author'
            if (!name) {
                propertyTitle = 'name'
            } else if (!price) {
                propertyTitle = 'price'
            }
            throw new NotEnoughProperties(propertyTitle)
        }
    }
    showInfo(list) {
        this.li.innerText = `Назва книги - ${this.name}; Автор - ${this.author}; Ціна - ${this.price}`
        list.append(this.li)
    }
}

books.forEach(element => {
    try {
        new BookInformation(element).showInfo(booksList)
    }
    catch (error) {
        if (error.errorName === "NotEnoughProperties") {
            console.error(error)
        } else {
            throw error;
        }
    }
})




























